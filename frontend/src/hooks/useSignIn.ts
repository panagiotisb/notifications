import { useContext, useEffect, useState } from "react"
import { SignedInState, getSignedInState, setSignedInStateValue } from "@/utils/signInUtils";
import { SocketMessagesContext } from "@/context/SocketMessagesContext";


const isSignedInStateFn = (state: SignedInState) => {
    return (state === SignedInState.SignedInConsumer || state === SignedInState.SignedInProducer)
}

interface SignInHookProps {
    signedInState: SignedInState;
    setSignedInState: (signInState: SignedInState) => void;
    username: string;
    setUsername: (username: string) => void;
}

export const useSignIn = ({signedInState, setSignedInState, username, setUsername}: SignInHookProps) => {
    const [isSignedInState, setIsSignedInState] = useState(false)
    const [isConsumer, setIsConsumer] = useState(false)
    const {resetStateCtx} = useContext(SocketMessagesContext)

    const signWithState = (state: SignedInState) => {
        setSignedInState(state)
        setSignedInStateValue(state)
    }

    const signOut = () => {
        if (isConsumer) { resetStateCtx() }
        signWithState(SignedInState.SignedOut)
        localStorage.setItem("username", "")
        setUsername("")
    }

    const signInProducer = () => {
        signWithState(SignedInState.SignedInProducer)
        localStorage.setItem("username", "producer")
        setUsername("producer")
    }

    const signInConsumer = (username: string) => {
        signWithState(SignedInState.SignedInConsumer)
        localStorage.setItem("username", username)
        setUsername(username)
    }

    useEffect(() => {
        const localStoredSignInState = getSignedInState()
        setSignedInState(localStoredSignInState)
        setUsername(localStorage.getItem("username") ?? "")
    })

    useEffect(() => {
        const isSignedInComputed = isSignedInStateFn(signedInState)
        setIsSignedInState(isSignedInComputed)
        setIsConsumer(signedInState === SignedInState.SignedInConsumer)
    }, [signedInState])

    return {
        isSignedInState, isConsumer,
        signOut, signInProducer, signInConsumer
    }
}
