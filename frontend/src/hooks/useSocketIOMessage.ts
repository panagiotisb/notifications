import { AuthContext } from "@/context/AuthContext"
import { MessageState, MessageStateAction } from "@/types/SocketMessage"
import { SignedInState } from "@/utils/signInUtils"
import { socketWithUsername } from "@/utils/socket"
import { Dispatch, useContext, useEffect, useState } from "react"


export const useSocketIOMessage = (socketMessageState: MessageState, dispatch: Dispatch<MessageStateAction>) => {
    const {latestMessage} = socketMessageState
    const {signInStateCtx, usernameCtx} = useContext(AuthContext)

    const [initStateTrigger, setInitStateTrigger] = useState({
        shouldTriggerInit: false,
        localMessage: "",
        localInbox: []
    })

    const [socket, setSocket] = useState(socketWithUsername(usernameCtx))
    const [socketShouldConnect, setsocketShouldConnect] = useState(socket.connected)

    useEffect(() => {
        const rawLocalMessage = localStorage.getItem("message")
        const rawLocalInbox = localStorage.getItem("inbox")
        if (rawLocalMessage === null) { localStorage.setItem("message", "") }
        if (rawLocalInbox === null) { localStorage.setItem("inbox", JSON.stringify([])) }
        const localInbox = rawLocalInbox
            ? JSON.parse(rawLocalInbox)
            : []

        const localMessage = rawLocalMessage === null ? "" : rawLocalMessage
        setInitStateTrigger({
            shouldTriggerInit: localMessage && localInbox,
            localMessage: localMessage,
            localInbox: localInbox
        })
    }, [])

    useEffect(() => {
        const {shouldTriggerInit, localMessage, localInbox} = initStateTrigger
        if (shouldTriggerInit) {
            dispatch({
                type: "INIT_MESSAGE_STATE", value: {
                    localMessage: localMessage,
                    localInbox: localInbox
                }
            })
        }
    }, [initStateTrigger, dispatch])

    useEffect(() => {
        setsocketShouldConnect(signInStateCtx === SignedInState.SignedInConsumer)
        if (signInStateCtx === SignedInState.SignedOut && initStateTrigger.shouldTriggerInit) {
            dispatch({type: "RESET_STATE"})
        }
    }, [signInStateCtx, dispatch, initStateTrigger])

    useEffect(() => {
        setSocket(socketWithUsername(usernameCtx))
    }, [usernameCtx])

    useEffect(() => {
        const {message} = latestMessage
        if (message) {
            localStorage.setItem("message", message)
        }
    }, [latestMessage, dispatch])

    useEffect(() => {
        const onSocketError = (e: Error) => { console.error(e.message) }
        const onProductAvailable = (payload: any) => {
            const { productId, message } = payload
            dispatch({
                type: "MESSAGE_RECEIVED", value: message
            })
            socket.emit(`roomUser:${usernameCtx}:${productId}`, "OK")
        }

        const onSocketConnect = () => {
            socket.on("roomUser", onProductAvailable)
            socket.on("disconnect", () => console.log("disconnect"))
            socket.on("error", onSocketError)
        }
        if (socketShouldConnect && !socket.connected) {
            socket.connect()
            socket.on("connect", onSocketConnect)
        } else {
            socket.disconnect()
        }

        return () => {
            socket.off("connect", onSocketConnect)
        }
    }, [socketShouldConnect, socket, dispatch, usernameCtx])
}
