import { MessageStatus, SocketMessagesContextI } from "@/types/SocketMessage";
import { createContext } from "react";


export const SocketMessagesContext = createContext<SocketMessagesContextI>({
    maxId: 0,
    messages: [],
    latestMessage: {
        id: 0,
        message: "",
        status: MessageStatus.READ
    },
    showInstantNotification: false,
    onRead: () => {},
    resetStateCtx: () => {}
})
