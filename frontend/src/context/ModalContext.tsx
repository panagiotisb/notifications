import { createContext } from "react";

interface ModalContextI {
    openCtx: boolean;
    onToggleCtx: () => void
}

export const ModalContext = createContext<ModalContextI>({
    openCtx: false,
    onToggleCtx: () => {}
})
