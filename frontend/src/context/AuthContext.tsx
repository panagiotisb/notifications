import { SignedInState } from "@/utils/signInUtils";
import { createContext } from "react";

interface AuthContextI {
    signInStateCtx: SignedInState;
    usernameCtx: string;
    setUsernameCtx: (usernameCtx: string) => void;
    setSignInStateCtx: (signInState: SignedInState) => void
}

export const AuthContext = createContext<AuthContextI>({
    signInStateCtx: SignedInState.SignedOut,
    usernameCtx: "",
    setUsernameCtx: () => {},
    setSignInStateCtx: () => {}
})
