import { initialSocketMessagesState } from "@/constants/messageConstants"
import { MessageState, MessageStateAction, MessageStatus } from "@/types/SocketMessage"
import _ from "lodash"

export default function messageReducer(messageState: MessageState, action: MessageStateAction) {
    const {type} = action
    switch(type) {
        case "MESSAGE_RECEIVED": {
            const {value} = action
            const {maxId, messages} = messageState
            const msgFound = _.find(messages, ({id, message, status}) => 
                id === maxId + 1 || (message === value && status === MessageStatus.UNREAD))
            if (msgFound) {
                return messageState
            } else {
                const latestMessageReceived = {
                    id: messageState.maxId + 1,
                    message: value,
                    status: MessageStatus.UNREAD
                }
                localStorage.setItem("inbox", JSON.stringify([
                    ...messageState.messages,
                    latestMessageReceived
                ]))
                localStorage.setItem("instant", "true")
                return {
                    maxId: messageState.maxId + 1,
                    messages: [
                        ...messageState.messages,
                        latestMessageReceived
                    ],
                    latestMessage: latestMessageReceived,
                    showInstantNotification: true
                }
            }
        }
        case "INIT_MESSAGE_STATE": {
            const inboxById = _.orderBy(action.value.localInbox, ['id'], 'desc')
            const initMaxId = inboxById.length > 0 ? inboxById[0].id : 0
            const initShowInstantMsg = JSON.parse(localStorage.getItem("instant") ?? "false")
            return {
                showInstantNotification: initShowInstantMsg,
                maxId: initMaxId,
                messages: inboxById,
                latestMessage: {
                    id: initMaxId,
                    message: action.value.localMessage,
                    status: MessageStatus.UNREAD
                }
            }
        }
        case "READ_MESSAGE": {
            const {value} = action
            const {messages} = messageState
            const nextMessages = _.map(messages, msg => msg.id === value ? {...msg, status: MessageStatus.READ} : msg)
            localStorage.setItem("inbox", JSON.stringify(nextMessages))
            return {
                ...messageState,
                messages: nextMessages
            }
        }
        case "CLOSE_NOTIFICATION": {
            localStorage.setItem("instant", "false")
            return {
                ...messageState,
                showInstantNotification: false
            }
        }
        case "SHOW_NOTIFICATION": {
            localStorage.setItem("instant", "true")
            return {
                ...messageState,
                showInstantNotification: true
            }
        }
        case "RESET_STATE": {
            localStorage.setItem("instant", "false")
            localStorage.setItem("message", "")
            localStorage.setItem("inbox", JSON.stringify([]))
            return initialSocketMessagesState
        }
    }
}
