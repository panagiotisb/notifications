import { useContext } from 'react'
import { ProductType } from '@/types/Product'
import ProductItem from './ProductItem'
import { useMutation, useQueryClient } from '@tanstack/react-query'
import axios from 'axios'
import GenericModal from './GenericModal'
import { ModalContext } from '@/context/ModalContext'


const PRODUCT_SERVICE_URL = "http://localhost:8001"
const SUBSCRIPTION_SERVICE_URL = "http://localhost:8002"

type FormInputType = string | number
type FormType = "AmountForm" | "SubscriptionForm"

interface ProductQuickViewProps {
    product: ProductType | null
}

type SubscriptionDetails = {
    name: string,
    email: string,
    productId: string
}

type UpdateProductDetails = {
    name?: string,
    description?: string,
    price?: number,
    amount?: number
}

export default function ProductQuickview({product}: ProductQuickViewProps) {
    const {onToggleCtx} = useContext(ModalContext)
    const queryClient = useQueryClient()

    const postSubscription = useMutation({
        mutationFn: (details: SubscriptionDetails) => {
            return axios.post(`${SUBSCRIPTION_SERVICE_URL}/subscriptions`, details)
        },
        onSuccess: (data, variables, context) => {
            onToggleCtx()
        }
    })
    const editAmount = useMutation({
        mutationFn: ({productId, details}: {productId: string, details: UpdateProductDetails}) => {
            return axios.put(`${PRODUCT_SERVICE_URL}/products/${productId}`, details)
        },
        onSuccess: (data, variables, context) => {
            queryClient.invalidateQueries({
                queryKey: ["products"],
                refetchType: "all"
            })
            onToggleCtx()
        }
    })

    const onProductFormSubmit = ({product, input, formType}: {product: ProductType, input: FormInputType, formType: FormType}) => {
        switch(formType) {
            case 'AmountForm': {
                editAmount.mutate({
                    productId: product._id,
                    details: {
                        name: product.name,
                        amount: input as number,
                        price: product.price
                    }
                })
                break
            }
            case 'SubscriptionForm': {
                postSubscription.mutate({
                    name: input as string,
                    email: input as string,
                    productId: product._id
                })
                break
            }
        }
    }

    return (
        <GenericModal>
            {product && <ProductItem product={product} onProductFormSubmit={onProductFormSubmit} />}
        </GenericModal>
    )
}
