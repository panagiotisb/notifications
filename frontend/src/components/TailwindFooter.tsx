export default function TailwindFooter() {
    return (
        <>
            <footer className="bg-white absolute bottom-0 shrink w-full shadow">
                <div className="w-screen mx-auto max-w-screen-xl p-4 flex md:flex md:items-center md:justify-between">
                    <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
                        NextJS Application: Services operate in PubSub, April 2023
                    </span>
                    <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
                        UI components from <em className="italic">TailwindCSS</em>
                    </span>
                </div>
            </footer>
        </>
    )
}
