import { useEffect, useState } from "react"
import ProductQuickview from "@/components/ProductQuickview"
import { ProductType } from "@/types/Product"
import { useQuery } from "@tanstack/react-query"
import axios from "axios"
import Image from "next/image"
import { ModalContext } from "@/context/ModalContext"

const PRODUCT_SERVICE_URL = "http://localhost:8001"


export default function Products() {
    const [toggle, setToggle] = useState(false)
    const [clickedProduct, setClickedProduct] = useState<string | null>(null)
    const [productModalValues, setProductModalValues] = useState<ProductType | null>(null)
    const {isLoading, data, isSuccess} = useQuery({
        queryKey: ["products"],
        queryFn: () => axios.get<[ProductType]>(`${PRODUCT_SERVICE_URL}/products`),
        select: (data) => {
            return data.data.map((product) => ({
                ...product,
                imageSrc: 'https://tailwindui.com/img/ecommerce-images/category-page-04-image-card-04.jpg',
                imageAlt: "Example Alternative"
            }))
        }
    })

    const onToggle = () => {
        setToggle(!toggle)
        setClickedProduct(null)
    }

    const onClickHandler = (productId: string) => {
        setClickedProduct(productId)
        setToggle(!toggle)
    }

    useEffect(() => {
        if(isSuccess) {
            const filteredProducts = data.filter(product => product._id === clickedProduct)
            const product = filteredProducts ? filteredProducts[0] : null
            setProductModalValues(product)
        }
    }, [clickedProduct, data, isSuccess])

    return (
        <>
            {isSuccess &&
            (<div className="bg-gray-200 grow min-h-screen">
                <div className="mx-auto max-w-2xl px-4 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
                    <h2 className="sr-only">Products</h2>

                    <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">   
                        {data.map((product) => (
                            <div key={product._id} className="group hover:cursor-pointer" onClick={() => onClickHandler(product._id)}>
                                <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-lg bg-gray-200 xl:aspect-h-8 xl:aspect-w-7">
                                    <Image
                                        src={product.imageSrc}
                                        alt={product.imageAlt}
                                        width={800}
                                        height={500}
                                        className="h-full w-full object-cover object-center group-hover:opacity-75"
                                        unoptimized
                                    />
                                </div>
                                <h3 className="mt-4 text-sm text-gray-700">{product.name}</h3>
                                <p className="mt-1 text-lg font-medium text-gray-900">&euro;{product.price}</p>
                            </div>
                        ))}
                    </div>
                    <ModalContext.Provider value={{openCtx: toggle, onToggleCtx: onToggle}}>
                        <ProductQuickview
                            product={productModalValues}
                        />
                    </ModalContext.Provider>
                </div>
            </div>)}
            {isLoading && 
            (<div className="bg-gray-200 grow min-h-screen">
                <div className="mx-auto max-w-2xl px-4 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
                    <span></span>
                </div>
            </div>)}
        </>
    )
}
