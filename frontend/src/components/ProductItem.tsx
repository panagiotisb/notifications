import { ProductType } from "@/types/Product";
import ProductForm from "./ProductForm";
import Image from "next/image";


type FormInputType = string | number
type FormType = "AmountForm" | "SubscriptionForm"

interface ProductItemProps {
    product: ProductType,
    onProductFormSubmit: ({product, input, formType}: {product: ProductType, input: FormInputType, formType: FormType}) => void
}

export default function ProductItem({product, onProductFormSubmit}: ProductItemProps) {
    const onFormSubmit = ({input, formType}: {input: FormInputType, formType: FormType}) => {
        if(input !== null && formType) {
            onProductFormSubmit({product, input, formType})
        }
    }

    return (
        <>
            <div className="grid w-full grid-cols-1 items-start gap-x-6 gap-y-8 sm:grid-cols-12 lg:gap-x-8">
                <div className="aspect-h-3 aspect-w-2 overflow-hidden rounded-lg bg-gray-100 sm:col-span-4 lg:col-span-5">
                    <Image src={product.imageSrc} alt={product.imageAlt} width={800} height={500} className="object-cover object-center" />
                </div>
                <div className="sm:col-span-8 lg:col-span-7 w-full gap-24 flex flex-wrap flex-col content-between">
                    <div>
                        <h2 className="text-2xl font-bold text-gray-900 sm:pr-12">{product.name}</h2>
                        <div aria-labelledby="information-heading" className="mt-2">    
                            <p className="text-2xl text-gray-900">&euro;{product.price}</p>
                        </div>
                    </div>

                    <div>
                        <ProductForm amount={product.amount.toString()} onFormSubmit={onFormSubmit} />
                    </div>
                </div>
            </div>
        </>
    )
}
