import { FormEvent, useState } from "react"

interface SubscriptionFormProps {
    onSubscription: (username: string) => void
}

export default function SubscriptionForm({onSubscription}: SubscriptionFormProps ) {
    const [input, setInput] = useState("")

    const onSubmitHandler = (e: FormEvent) => {
        e.preventDefault()
        onSubscription(input)
    }

    return (
        <>
            <form onSubmit={e => onSubmitHandler(e)}>
                <div className="sm:col-span-4">
                    <label htmlFor="username" className="block text-sm font-medium leading-6 text-gray-900">
                        Username
                    </label>
                    <div className="mt-2">
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                            <input
                                type="text"
                                name="username"
                                id="username"
                                autoComplete="off"
                                value={input}
                                onInput={e => setInput(e.currentTarget.value)}
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder="enter your username"
                            />
                        </div>
                    </div>
                </div>
                <button
                    type="submit"
                    className="mt-6 flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-8 py-3 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                >   Request notification
                </button>
            </form>
        </>
    )
}
