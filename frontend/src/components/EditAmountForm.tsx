import { FormEvent, useState } from "react"


interface EditAmountFormProps {
    amount: string,
    onEditAmount: (amount: number) => void
}

export default function EditAmountForm({amount, onEditAmount}: EditAmountFormProps) {
    const [input, setInput] = useState(amount)

    const onSubmitHandler = (e: FormEvent) => {
        e.preventDefault()
        const valueNumber = parseInt(input, 10)
        !isNaN(valueNumber) 
            ? onEditAmount(valueNumber)
            : setInput("")
    }

    const onInputHandler = (value: string) => {
        setInput(value)
    }

    return (
        <>
            <form onSubmit={e => onSubmitHandler(e)}>
                <div className="sm:col-span-4">
                    <label htmlFor="username" className="block text-sm font-medium leading-6 text-gray-900">
                        Amount
                    </label>
                    <div className="mt-2">
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                            <input
                                type="number"
                                name="amount"
                                id="amount"
                                autoComplete="off"
                                value={input}
                                onInput={e => onInputHandler(e.currentTarget.value)}
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder={amount}
                            />
                        </div>
                    </div>
                </div>
                <button
                    type="submit"
                    className="mt-6 flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-8 py-3 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                >   Edit amount
                </button>
            </form>
        </>
    )
}
