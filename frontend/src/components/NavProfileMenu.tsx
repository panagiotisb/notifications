import { useSignIn } from "@/hooks/useSignIn";
import { Menu, Transition } from "@headlessui/react";
import { Fragment, useContext, useState } from "react";
import SignInModal from "@/components/SignInModal";
import { SignedInState } from "@/utils/signInUtils";
import AddProductModal from "@/components/AddProductModal";
import { AuthContext } from "@/context/AuthContext";
import Image from "next/image";
import { ModalContext } from "@/context/ModalContext";
import { classNames } from "@/utils/css";


export default function NavProfileMenu() {
    const {signInStateCtx, setSignInStateCtx, usernameCtx, setUsernameCtx} = useContext(AuthContext)
    const {isSignedInState, signOut, signInProducer} = useSignIn({
        signedInState: signInStateCtx,
        setSignedInState: setSignInStateCtx,
        username: usernameCtx,
        setUsername: setUsernameCtx
    })
    const [openSignInModal, setOpenSignInModal] = useState(false)
    const [openAddProductModal, setOpenAddProductModal] = useState(false)

    const onSignInModalToggle = () => { setOpenSignInModal(false) }
    const onClickAddProduct = () => { setOpenAddProductModal(true) }
    const onAddProductModalToggle = () => { setOpenAddProductModal(false) }
    const onClickSignInConsumer = () => { setOpenSignInModal(true) }

    const onClickSignInProducer = () => { signInProducer() }
    const onClickSignOut = () => { signOut() }

    const onSubmitProduct = (productId: string) => { console.log(productId) }

    return (
        <Menu as="div" className="relative ml-3">
            <div>
                <Menu.Button className="flex rounded-full bg-gray-800 text-sm focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800">
                    <span className="sr-only">Open user menu</span>
                    <Image
                        className="h-8 w-8 rounded-full"
                        src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                        alt=""
                        width={800}
                        height={500}
                        unoptimized
                    />
                </Menu.Button>
            </div>
            <ModalContext.Provider value={{openCtx: openSignInModal, onToggleCtx: onSignInModalToggle}}>
                <SignInModal />
            </ModalContext.Provider>
            <ModalContext.Provider value={{openCtx: openAddProductModal, onToggleCtx: onAddProductModalToggle}}>
                <AddProductModal onSubmitProduct={onSubmitProduct} />
            </ModalContext.Provider>
            <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
            >
                <Menu.Items className="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                    {!isSignedInState && 
                    (<Menu.Item>
                        {({ active }) => (
                            <a
                                className={classNames(active ? 'bg-gray-100 hover:cursor-pointer' : '', 'block px-4 py-2 text-sm text-gray-700')}
                                onClick={onClickSignInProducer}
                            >
                                Sign as Producer
                            </a>
                        )}
                    </Menu.Item>)}
                    {!isSignedInState && 
                    (<Menu.Item>
                        {({ active }) => (
                            <a
                                className={classNames(active ? 'bg-gray-100 hover:cursor-pointer' : '', 'block px-4 py-2 text-sm text-gray-700')}
                                onClick={onClickSignInConsumer}
                            >
                                Sign as Consumer
                            </a>
                        )}
                    </Menu.Item>)}
                    {signInStateCtx === SignedInState.SignedInProducer &&
                    (<Menu.Item>
                        {({ active }) => (
                            <a
                                className={classNames(active ? 'bg-gray-100 hover:cursor-pointer' : '', 'block px-4 py-2 text-sm text-gray-700')}
                                onClick={onClickAddProduct}
                            >
                                Add Product
                            </a>
                        )}
                    </Menu.Item>)}
                    {isSignedInState &&
                    (<Menu.Item>
                        {({ active }) => (
                            <a
                                className={classNames(active ? 'bg-gray-100 hover:cursor-pointer' : '', 'block px-4 py-2 text-sm text-gray-700')}
                                onClick={onClickSignOut}
                            >
                                Sign out
                            </a>
                        )}
                    </Menu.Item>)}
                </Menu.Items>
            </Transition>
        </Menu>
    )
}
