import { LockClosedIcon } from '@heroicons/react/20/solid'
import { FormEvent, useState } from 'react'
import axios from "axios"
import { useMutation, useQueryClient } from '@tanstack/react-query'


const PRODUCT_SERVICE_URL = "http://localhost:8001"

type ProductInput = {
    name: string,
    description: string,
    price: number,
    amount: number
}

interface SignInProps {
    onProductSubmit: (product: string) => void
}

const toIntegerStateValue = (raw: string) => {
    const numberInput = parseInt(raw, 10)
    return !isNaN(numberInput) ? numberInput : 0
}

export default function AddProductForm({onProductSubmit}: SignInProps) {
    const [productName, setProductName] = useState("")
    const [price, setPrice] = useState<number | null>(null)
    const [amount, setAmount] = useState<number | null>(null)

    const queryClient = useQueryClient()

    const postProduct = useMutation({
        mutationFn: (newProduct: ProductInput) => {
            return axios.post(`${PRODUCT_SERVICE_URL}/products`, newProduct)
        },
        onSuccess: (data, variables, context) => {
            onProductSubmit(data.data._id)
            queryClient.invalidateQueries({
                queryKey: ["products"]
            })
        }
    })

    const onSubmitHandler = (e: FormEvent) => {
        e.preventDefault()
        postProduct.mutate({
            name: productName,
            description: "This is a product",
            price: price ?? 0,
            amount: amount ?? 0
        })
    }

    return (
        <>
            <div className="w-full">
                <div className="flex min-h-full items-center justify-center px-4 py-12 sm:px-6 lg:px-8">
                    <div className="w-full max-w-md space-y-8">
                        <div>
                            <h2 className="mt-6 text-center text-3xl font-bold tracking-tight text-gray-900">
                                Add Product
                            </h2>
                        </div>
                        <form className="mt-8 space-y-6" onSubmit={e => onSubmitHandler(e)}>
                            <input type="hidden" name="remember" defaultValue="true" />
                            <div className="-space-y-px rounded-md shadow-sm">
                                <div>
                                    <label htmlFor="email-address" className="sr-only">
                                        Email address
                                    </label>
                                    <input
                                        id="product-name"
                                        name="product-name"
                                        type="text"
                                        autoComplete="off"
                                        value={productName}
                                        onInput={e => setProductName(e.currentTarget.value)}
                                        required
                                        className="relative bg-white block w-full rounded-t-md border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        placeholder="Product Name"
                                    />
                                </div>
                                <div>
                                    <label htmlFor="email-address" className="sr-only">
                                        Price
                                    </label>
                                    <input
                                        id="product-price"
                                        name="product-price"
                                        type="number"
                                        autoComplete="off"
                                        value={price ?? ""}
                                        onInput={e => setPrice(toIntegerStateValue(e.currentTarget.value))}
                                        required
                                        className="relative bg-white block w-full rounded-t-md border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        placeholder="Product Price"
                                    />
                                </div>
                                <div>
                                    <label htmlFor="email-address" className="sr-only">
                                        Amount
                                    </label>
                                    <input
                                        id="product-price"
                                        name="product-price"
                                        type="number"
                                        autoComplete="off"
                                        value={amount ?? ""}
                                        onInput={e => setAmount(toIntegerStateValue(e.currentTarget.value))}
                                        required
                                        className="relative bg-white block w-full rounded-t-md border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        placeholder="Product Amount"
                                    />
                                </div>
                            </div>

                            <div>
                                <button
                                    type="submit"
                                    className="group relative flex w-full justify-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                                        <LockClosedIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" aria-hidden="true" />
                                    </span>
                                    Add product
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}
