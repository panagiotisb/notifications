import Head from "next/head"
import NavigationBar from "@/components/NavigationBar"
import TailwindFooter from "@/components/TailwindFooter"
import NotificationLayout from "./NotificationLayout"


export default function MainLayout({ children }: React.PropsWithChildren<{}>) {
    return (
        <>
            <Head>
                <title>FullStack Application in Node/Next</title>
                <meta name="description" content="Sample Frontend NextJS Application" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className="relative min-h-screen">
                <NotificationLayout>
                    <NavigationBar />
                    <main className="pb-3">
                        {children}
                    </main>
                    <TailwindFooter />
                </NotificationLayout>
            </div>
        </>
    )
}
