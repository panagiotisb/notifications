import { useContext } from "react";
import AddProductForm from "./AddProductForm";
import { ModalContext } from "@/context/ModalContext";
import GenericModal from "./GenericModal";


interface SignInModalProps {
    onSubmitProduct: (productId: string) => void
}

export default function AddProductModal({onSubmitProduct}: SignInModalProps) {
    const {onToggleCtx} = useContext(ModalContext)

    const onProductSubmit = (productId: string) => {
        onSubmitProduct(productId)
        onToggleCtx()
    }

    return (
        <GenericModal>
            <AddProductForm onProductSubmit={onProductSubmit} />
        </GenericModal>
    )
}
