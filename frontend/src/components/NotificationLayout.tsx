import { useSocketIOMessage } from "@/hooks/useSocketIOMessage"
import NotificationAlert from "@/components/NotificationAlert"
import { SocketMessagesContext } from "@/context/SocketMessagesContext"
import { useContext, useMemo, useReducer } from "react"
import {initialSocketMessagesState} from "@/constants/messageConstants"
import messageReducer from "@/reducers/messageReducer"
import { AuthContext } from "@/context/AuthContext"
import { SignedInState } from "@/utils/signInUtils"
import { classNames } from "@/utils/css"


export default function NotificationLayout({ children }: React.PropsWithChildren) {
    const {signInStateCtx} = useContext(AuthContext)
    const [socketMessagesState, dispatch] = useReducer(messageReducer, initialSocketMessagesState)
    useSocketIOMessage(socketMessagesState, dispatch)

    const onRead = (id: number) => dispatch({type: "READ_MESSAGE", value: id})
    const resetStateCtx = () => dispatch({type: "RESET_STATE"})
    const socketMsgCtx = useMemo(() => {
        return {
            ...socketMessagesState,
            onRead: onRead,
            resetStateCtx: resetStateCtx
        }
    }, [socketMessagesState])

    return (
        <>
            <SocketMessagesContext.Provider value={socketMsgCtx}>
                {socketMessagesState.showInstantNotification && signInStateCtx === SignedInState.SignedInConsumer &&
                    <NotificationAlert
                        message={socketMessagesState.latestMessage.message}
                        onNotificationClose={() => dispatch({type: "CLOSE_NOTIFICATION"})}
                    />}
                <div className={classNames(
                    socketMessagesState.showInstantNotification && signInStateCtx === SignedInState.SignedInConsumer
                        ? "mt-8"
                        : "mt-0",
                    "top-16 flex flex-col"
                )}>
                    {children}
                </div>
            </SocketMessagesContext.Provider>
        </>
    )
}
