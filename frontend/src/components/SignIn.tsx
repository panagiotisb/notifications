import { LockClosedIcon } from '@heroicons/react/20/solid'
import { FormEvent, useState } from 'react'


interface SignInProps {
    onUsernameSubmit: (username: string) => void
}

export default function SignIn({onUsernameSubmit}: SignInProps) {
    const [usernameInput, setUsernameInput] = useState<string>("")

    const onSubmitHandler = (e: FormEvent) => {
        e.preventDefault()
        onUsernameSubmit(usernameInput)
    }

    return (
        <>
            <div className="w-full">
                <div className="flex min-h-full items-center justify-center px-4 py-12 sm:px-6 lg:px-8">
                    <div className="w-full max-w-md space-y-8">
                        <div>
                            <h2 className="mt-6 text-center text-3xl font-bold tracking-tight text-gray-900">
                                Sign in
                            </h2>
                        </div>
                        <form className="mt-8 space-y-6" onSubmit={e => onSubmitHandler(e)}>
                            <input type="hidden" name="remember" defaultValue="true" />
                            <div className="-space-y-px rounded-md shadow-sm">
                                <div>
                                    <label htmlFor="email-address" className="sr-only">
                                        Email address
                                    </label>
                                    <input
                                        id="username-producer"
                                        name="username"
                                        type="text"
                                        autoComplete="off"
                                        value={usernameInput}
                                        onInput={e => setUsernameInput(e.currentTarget.value)}
                                        required
                                        className="relative bg-white block w-full rounded-t-md border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        placeholder="Username"
                                    />
                                </div>
                            </div>

                            <div>
                                <button
                                    type="submit"
                                    className="group relative flex w-full justify-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                                        <LockClosedIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" aria-hidden="true" />
                                    </span>
                                    Sign in
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}
