import { SignedInState } from "@/utils/signInUtils";
import EditAmountForm from "./EditAmountForm";
import SubscriptionForm from "./SubscriptionForm";
import { useContext } from "react";
import { AuthContext } from "@/context/AuthContext";
import { FormActionType, FormInputType, ProductFormProps } from "@/types/ProductFormTypes";


export default function ProductForm({amount, onFormSubmit}: ProductFormProps) {
    const { signInStateCtx } = useContext(AuthContext)

    const formSubmit = (input: FormInputType, formType: FormActionType) => {
        if(input !== null) {
            onFormSubmit({
                input: input,
                formType: formType as FormActionType
            })
        }
    }

    const onEditAmountFormAction = (value: number) => formSubmit(value, FormActionType.AmountForm)
    const onSubscriptionFormAction = (value: string) => formSubmit(value, FormActionType.SubscriptionForm)

    return (
        <>
            {signInStateCtx === SignedInState.SignedInProducer &&
                <EditAmountForm amount={amount} onEditAmount={onEditAmountFormAction} />}

            {signInStateCtx === SignedInState.SignedInConsumer &&
                <SubscriptionForm onSubscription={onSubscriptionFormAction} />}
        </>
    )
}
