import { useContext } from "react";
import SignIn from "@/components/SignIn";
import { AuthContext } from "@/context/AuthContext";
import { useSignIn } from "@/hooks/useSignIn";
import GenericModal from "@/components/GenericModal";
import { ModalContext } from "@/context/ModalContext";


export default function SignInModal() {
    const {onToggleCtx} = useContext(ModalContext)
    const {signInStateCtx, setSignInStateCtx, usernameCtx, setUsernameCtx} = useContext(AuthContext)
    const {signInConsumer} = useSignIn({
        signedInState: signInStateCtx,
        setSignedInState: setSignInStateCtx,
        username: usernameCtx,
        setUsername: setUsernameCtx
    })

    const onUsernameSubmit = (username: string) => {
        signInConsumer(username)
        onToggleCtx()
    }

    return (
        <GenericModal>
            <SignIn onUsernameSubmit={onUsernameSubmit}/>
        </GenericModal>
    )
}
