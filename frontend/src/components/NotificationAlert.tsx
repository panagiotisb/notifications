import { XMarkIcon } from "@heroicons/react/24/outline";
import { MouseEvent } from "react";


interface NotificationAlertProps {
    message: string,
    onNotificationClose: () => void
}

export default function NotificationAlert({ message, onNotificationClose }: NotificationAlertProps) {
    
    const onClose = (e: MouseEvent) => {
        e.preventDefault()
        onNotificationClose()
    }

    return (
        <div className="z-10 animate-marquee mt-12 absolute p-2 bg-indigo-800 items-center text-indigo-100 leading-none lg:rounded-full flex lg:inline-flex" role="alert">
            <span className="flex rounded-full bg-indigo-500 uppercase px-2 py-1 text-xs font-bold mr-3">New</span>
            <span className="font-semibold mr-2 text-left flex-auto">{message}</span>
            <div>
                <button
                    type="button"
                    className="text-gray-400 hover:cursor-pointer sm:right-6 sm:top-8 md:right-6 md:top-6 lg:right-8 lg:top-8"
                    onClick={e => onClose(e)}
                >
                    <span className="sr-only">Close</span>
                    <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                </button>
            </div>
        </div>
    )
}