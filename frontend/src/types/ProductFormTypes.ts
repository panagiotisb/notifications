export type FormInputType = string | number

export type ResetForm = "ResetForm"
export enum FormActionType {
    AmountForm = "AmountForm",
    SubscriptionForm = "SubscriptionForm"
}

export type FormAction = {
    type: FormActionType | ResetForm,
    value: FormInputType | null
}

export interface FormState {
    input: FormInputType | null
    formType: FormActionType | ResetForm
}

export interface ProductFormProps {
    amount: string,
    onFormSubmit: ({input, formType}: {input: FormInputType, formType: FormActionType}) => void
}
