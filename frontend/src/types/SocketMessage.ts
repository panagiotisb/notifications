export type INIT_MESSAGE_STATE = "INIT_MESSAGE_STATE"
export type MESSAGE_RECEIVED = "MESSAGE_RECEIVED"
export type READ_MESSAGE = "READ_MESSAGE"
export type CLOSE_NOTIFICATION = "CLOSE_NOTIFICATION"
export type SHOW_NOTIFICATION = "SHOW_NOTIFICATION"
export type RESET_STATE = "RESET_STATE"

export type MessageStateAction = {
    type: MESSAGE_RECEIVED,
    value: string 
} | {
    type: INIT_MESSAGE_STATE,
    value: LocalMessageState
} | {
    type: READ_MESSAGE,
    value: number
} | {
    type: CLOSE_NOTIFICATION
} | {
    type: SHOW_NOTIFICATION
} | {
    type: RESET_STATE
}

type LocalMessageState = {
    localMessage: string,
    localInbox: Message[]
}

export type MessageState = {
    maxId: number;
    messages: Message[];
    latestMessage: Message;
    showInstantNotification: boolean;
}

export enum MessageStatus {
    READ = "READ",
    UNREAD = "UNREAD"
}

export interface Message {
    id: number;
    message: string;
    status: MessageStatus
}

export interface SocketMessagesContextI {
    maxId: number;
    messages: Message[];
    latestMessage: Message;
    showInstantNotification: boolean;
    onRead: (id: number) => void;
    resetStateCtx: () => void
}
