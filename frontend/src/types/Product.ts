export type ProductType = {
    _id: string,
    name: string,
    description: string,
    price: number,
    amount: number,
    imageSrc: string,
    imageAlt: string
}
