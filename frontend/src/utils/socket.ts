import { io } from "socket.io-client"

const URL = "ws://localhost:8002"
export const socketWithUsername = (username: string) => io(URL, {
    autoConnect: false,
    extraHeaders: {
        token: username
    }
})
