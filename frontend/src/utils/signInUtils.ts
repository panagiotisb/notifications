export enum SignedInState {
    SignedOut = "SignedOut",
    SignedInProducer = "SignedInProducer",
    SignedInConsumer = "SignedInConsumer"
}

export const signedInStates = [
    "SignedOut",
    "SignedInConsumer", 
    "SignedInProducer"
]

export type Nullable<T> = T | null

export const toValidSignedInState = (value: string): Nullable<SignedInState> => {
    return signedInStates.includes(value) ? value as SignedInState : null
}

export const getSignedInState = (): SignedInState => {
    const signedInLocalStorage = localStorage.getItem("signedIn")
    const signedInState = signedInLocalStorage 
        ? toValidSignedInState(signedInLocalStorage) ?? SignedInState.SignedOut
        : SignedInState.SignedOut
    setSignedInStateValue(signedInState)
    return signedInState
}

export const setSignedInStateValue = (value: SignedInState) => {
    localStorage.setItem("signedIn", value)
}
