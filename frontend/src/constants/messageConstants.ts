import { MessageStatus } from "@/types/SocketMessage";

export const initialSocketMessagesState = {
    maxId: 0,
    messages: [],
    latestMessage: {
        id: 0,
        message: "",
        status: MessageStatus.UNREAD
    },
    showInstantNotification: false
}
