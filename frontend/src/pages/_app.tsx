import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import MainLayout from '@/components/MainLayout'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { AuthContext } from '@/context/AuthContext'
import { useSignIn } from '@/hooks/useSignIn'
import { useMemo, useState } from 'react'
import { SignedInState } from '@/utils/signInUtils'

const queryClient = new QueryClient()

export default function App({ Component, pageProps }: AppProps) {
  const [signInStateCtxST, setSignInStateCtxST] = useState(SignedInState.SignedOut)
  const [usernameCtxST, setUsernameCtxST] = useState("")

  useSignIn({
    signedInState: signInStateCtxST,
    setSignedInState: setSignInStateCtxST,
    username: usernameCtxST,
    setUsername: setUsernameCtxST
  })

  const initialCtx = useMemo(() => ({
    signInStateCtx: signInStateCtxST,
    usernameCtx: usernameCtxST,
    setUsernameCtx: setUsernameCtxST,
    setSignInStateCtx: setSignInStateCtxST
  }), [signInStateCtxST, usernameCtxST])

  return (
    <AuthContext.Provider value={initialCtx}>
      <QueryClientProvider client={queryClient}>
        <MainLayout>
          <Component {...pageProps} />
        </MainLayout>
      </QueryClientProvider>
    </AuthContext.Provider>
  )
}
