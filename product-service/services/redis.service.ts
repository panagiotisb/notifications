import { redisPublishClient } from "../utils/redis"


export const sendAvailabilityEvent = async (id: string) => {
    try {
        await redisPublishClient.publish(id, 'availability')
    } catch (error) {
        throw new Error("Redis publish error")
    }
}
