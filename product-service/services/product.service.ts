import { HydratedDocument } from "mongoose";
import Product from "../models/product.models";
import {sendAvailabilityEvent} from "./redis.service";
import { ResourceNotFoundError } from "../exceptions/ResourceNotFoundException";
import { ProductInterface } from "../types/productInteface";
import { UpdateParams } from "../types/utils";

export const createProduct = (product: ProductInterface) => {
    const productDocument: HydratedDocument<ProductInterface> = new Product(product)
    return productDocument.save()
}

export const updateProduct = async (id: string, updateParams: UpdateParams<ProductInterface>) => {
    return await Product
        .findByIdAndUpdate(id, updateParams, {
            new: true
        })
}

export const getOne = async (id: string) => {
    return await Product
        .findById(id)
        .orFail(new ResourceNotFoundError(`Resource product with id ${id} not found.`))
}

export const getAll = async () => {
    return await Product.find()
}

export const checkIfAvailableAgain = async (id: string, updateParams: UpdateParams<ProductInterface>) => {
    const product = await getOne(id)
    if(product !== null && product.amount === 0 && updateParams.amount > 0) {
        await sendAvailabilityEvent(id)
    }
}
