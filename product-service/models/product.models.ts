import {model, Schema} from "mongoose"
import { ProductInterface } from "../types/productInteface"

const productSchema = new Schema<ProductInterface>({
    name: { type: String, required: true },
    description: String,
    price: { type: Number, required: true },
    amount: { type: Number, required: true }
})

const Product = model<ProductInterface>('Product', productSchema)
export default Product
