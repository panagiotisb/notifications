import { body, param } from "express-validator";
import { Types } from "mongoose";

export const productValidators = [
    body("name")
        .notEmpty()
        .withMessage("Product name is required."),
    body("description")
        .optional(),
    body("price")
        .notEmpty()
        .withMessage("Price is required"),
    body("amount")
        .notEmpty()
        .withMessage("Amount is required."),
]

export const productIdValidator = param("id")
    .custom((val: string) => {
        if (!Types.ObjectId.isValid(val)) {
            throw new Error(`Mongoose ID [${val}] is not valid.`)
        }
        return val
})