import Product from "../../models/product.models"

export const data = [
    {
        name: "Test Product One",
        description: "Not to be found after the test",
        price: 10,
        amount: 100
    },
    {
        name: "Test Product Two",
        description: "Not to be found after the test",
        price: 12,
        amount: 21
    },
    {
        name: "Test Product Three",
        description: "Not to be found after the test",
        price: 11,
        amount: 11
    },
    {
        name: "Test Product Four",
        description: "Not to be found after the test",
        price: 42,
        amount: 98
    },
    {
        name: "Test Product Five",
        description: "Not to be found after the test",
        price: 24,
        amount: 17
    },
    {
        name: "Test Product Six",
        description: "Not to be found after the test",
        price: 91,
        amount: 19
    },
    {
        name: "Test Product Seven",
        description: "Not to be found after the test",
        price: 10,
        amount: 21
    }
]

export const loadDatabase = async () => {
    for (const el of data) {
        const product = new Product(el)
        await product.save()
    }
}

