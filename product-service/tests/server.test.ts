import request from "supertest"

import { app } from "../ServerApplication"
import { test, describe, expect, beforeAll, afterAll } from "@jest/globals"
import mongoose, { Connection, connect } from "mongoose";
import { ProductInterface } from "../types/productInteface";
import _ from "lodash";
import Product from "../models/product.models";
import { MongoMemoryServer } from "mongodb-memory-server";
import { data, loadDatabase } from "./utils/data.utils";


type ProductWithId = ProductInterface & { _id: string }


mongoose.set('strictQuery', false)
describe("Product APIs test", () => {
    const mongod = MongoMemoryServer.create();
    let mongooseConnection: Connection;

    beforeAll(async () => {
        const mongoUri = (await mongod).getUri();
        mongooseConnection = (await connect(mongoUri)).connection
    });

    test("POST /products returns 201 CREATED", async () => {
        await request(app)
            .post("/products")
            .send({
                name: "Product Foo",
                description: "Not to be found after the test",
                price: 20,
                amount: 11
            })
            .accept("application/json")
            .expect(201)
    })

    test("POST /products returns 400 BAD REQUEST", async () => {
        await request(app)
            .post("/products")
            .send({
                name: "Product Foo",
                description: "Not to be found after the test"
            })
            .accept("application/json")
            .expect(400)
    })

    test("GET /products returns 200 OK", async () => {
        await loadDatabase()
        const response = await request(app).get("/products")
        expect(response.statusCode).toBe(200)
        expect(response.body).toHaveLength(data.length)
    })

    test("GET /products/{id} returns 200 OK", async () => {
        await loadDatabase()
        const response = await request(app).get("/products").expect(200)
        const products: ProductWithId[] = response.body as ProductWithId[]
        for (const el of products) {
            const getProductWithIdResponse = await request(app)
                .get(`/products/${el._id.toString()}`)
                .expect(200)
            const product: ProductWithId = getProductWithIdResponse.body as ProductWithId
            expect(_.isEqual(product, products.find(p => p._id === product._id))).toBeTruthy()
        }
    })

    test("PUT /products/{id} returns 200 OK", async () => {
        await loadDatabase()
        const productsRes = await request(app).get("/products")
        const products = productsRes.body as ProductWithId[]
        for (const product of products) {
            const updatedProductRes = await request(app)
                .put(`/products/${product._id}`)
                .send({
                    name: product.name + " Updated",
                    description: product.description + " Updated",
                    price: product.price + 1,
                    amount: product.amount + 1
                })
                .accept("application/json")
                .expect(200);
            const updatedProduct: ProductWithId = updatedProductRes.body as ProductWithId;
            expect(updatedProduct._id).toBe(product._id);
            expect(updatedProduct.name).toBe(product.name + " Updated");
            expect(updatedProduct.description).toBe(product.description + " Updated");
            expect(updatedProduct.price).toBe(product.price + 1);
            expect(updatedProduct.amount).toBe(product.amount + 1);
        }
    })

    test("PUT /products/{id} returns 400 BAD REQUEST", async () => {
        await loadDatabase()
        const productsRes = await request(app).get("/products")
        const products = productsRes.body as ProductWithId[]
        for (const product of products) {
            const id = product._id
            await request(app)
                .put(`/products/${id}`)
                .send({
                    name: product.name + " Updated",
                    description: product.description + " Updated"
                })
                .accept("application/json")
                .expect(400);
            const currentProductResponse = await request(app).get(`/products/${id}`)
            const currentProduct: ProductWithId = currentProductResponse.body as ProductWithId
            expect(_.isEqual(currentProduct, product)).toBeTruthy()
        }
    })

    test("PUT /products/{id} returns 404 NOT FOUND", async () => {
        await loadDatabase()

        const id = new mongoose.Types.ObjectId(1)
        await request(app)
            .put(`/products/${id.toString()}`)
            .send({
                name: "Updated",
                description: " Updated",
                price: 1,
                amount: 1
            })
            .accept("application/json")
            .expect(404);
    })

    test("GET /products/{id} returns 404", async () => {
        await loadDatabase()
        const notExistingObjectId = new mongoose.Types.ObjectId(1)
        await request(app)
            .get(`/products/${notExistingObjectId.toString()}`)
            .expect(404)
    })

    test("GET /products/{id} returns 422", async () => {
        const notExistingObjectId = "notExists"
        await request(app)
            .get(`/products/${notExistingObjectId}`)
            .expect(422)
    })

    afterEach(async () => {
        await Product.deleteMany({})
    })

    afterAll(async () => {
        await mongooseConnection.close()
        await (await mongod).stop()
    })
});
