import dotenv from 'dotenv'


type Optional<T> = T | undefined
const getEnvPath = (env: Optional<string>) => {
    switch (env) {
        case "local":
            return ""
        case "compose":
            return ".compose";
        case "test":
            return ".test"
    }
}

const envPath = getEnvPath(process.env.NODE_ENV)

dotenv.config({
    path: `${envPath}.env`
})

export const mongoURL = () => {
    const {
        MONGO_USER,
        MONGO_PASSWORD,
        MONGO_HOST,
        MONGO_PORT
    } = process.env
    if (MONGO_USER === undefined || 
        MONGO_PASSWORD === undefined || 
        MONGO_HOST === undefined || 
        MONGO_PORT === undefined) {
        throw new Error("Env not set correctly.")
    }
    return `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}`
}

const getPort = () => {
    const {
        PORT
    } = process.env
    if (PORT === undefined) {
        throw Error("Env not set correctly (port)")
    }
    return Number(PORT)
}

const getRedisConfig = () => {
    const {
        REDIS_HOST,
        REDIS_PORT
    } = process.env
    if (REDIS_HOST === undefined || REDIS_PORT === undefined) {
        throw Error("Env not set correctly (redis)")
    }
    return {
        host: REDIS_HOST,
        port: Number(REDIS_PORT)
    }
}

export const appConfig = {
    mongoURL: mongoURL(),
    port: getPort(),
    redisConfig: getRedisConfig()
}
