import mongoose, {connect} from "mongoose";
import { appConfig } from "../config/config";


mongoose.set('strictQuery', false)
export const createMongoConnection = async () => {
    const {mongoURL} = appConfig
    const mongooseConnection = await connect(mongoURL)
    mongooseConnection.connection
        .on('connected', () => {
            console.log('[Redis] Database connected')
        })
        .on('error', () => {
            throw new Error("Mongo Connection")
        })
}
