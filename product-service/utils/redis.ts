import { createClient } from "redis"
import { appConfig } from "../config/config"


export const redisPublishClient = createClient({
    socket: appConfig.redisConfig
})

export const connectPublishClient = async () => {
    try {
        await redisPublishClient.connect()
    } catch (error) {
        throw new Error("Redis connection error")
    }
}
