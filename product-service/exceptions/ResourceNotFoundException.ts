export class ResourceNotFoundError extends Error {
    name: string
    constructor(message: string) {
        super(message)
        this.name = "ResourceNotFoundError"
    }
}