import {createMongoConnection} from "./utils/mongo";
import {connectPublishClient} from "./utils/redis";
import { app } from "./ServerApplication";
import { appConfig } from './config/config';


const { port } = appConfig

Promise.all([
  createMongoConnection(),
  connectPublishClient()
])
.then(() => app.listen(port, () => {
  console.log(`[server] [Products] Service is running at http://localhost:${port}`)
}))
.catch(error => console.error(error))
