import cors from "cors";
import express, { Express } from "express";

import productRouter from './routes/product.routes'


const allowedOrigins = ['http://localhost:3000'];
const options: cors.CorsOptions = {
  origin: allowedOrigins
};


export const app: Express = express()
app.use(cors(options))
app.use(express.json())
app.use('/products', productRouter)
