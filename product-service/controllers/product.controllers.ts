import {Request, Response} from "express"
import {checkIfAvailableAgain, createProduct, getAll, getOne, updateProduct} from "../services/product.service"
import { validationResult } from "express-validator"
import { ResourceNotFoundError } from "../exceptions/ResourceNotFoundException"
import { ProductRequestT } from "../types/requests.types" 
import { ProductInterface } from "../types/productInteface"

export const getProduct = async (req: Request, res: Response) => {
    const id = req.params.id
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(422).json(errors)
        return
    }

    try {
        const product = await getOne(id)
        res.status(200).json(product)
    } catch (error) {
        if (error instanceof ResourceNotFoundError) {
            res.status(404).json(error.message)
        }
    }
}

export const getAllProducts = async (req: Request, res: Response) => {
    const products = await getAll()
    res.status(200).json(products)
}

export const postProduct = (req: ProductRequestT, res: Response) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(400).json(errors)
        return
    } 

    const { name, description, price, amount } = req.body
    const product: ProductInterface = {
        name: name,
        description: description,
        price: price,
        amount: amount
    }

    const savedData = createProduct(product)
    res.status(201).json(savedData)
}

export const update = async (req: ProductRequestT, res: Response) => {
    const id: string = req.params.id
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(400).json(errors)
        return
    }

    const { name, description, price, amount } = req.body
    const updateParams = { name, description, price, amount }

    try {
        await checkIfAvailableAgain(id, updateParams)
        const product = await updateProduct(id, updateParams)
        res.status(200).json(product)
    } catch (error) {
        if (error instanceof ResourceNotFoundError) {
            res.status(404).json({message: error.message})
        }
    }
}
