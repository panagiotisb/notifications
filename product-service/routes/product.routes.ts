import express from "express";
import {getAllProducts, getProduct, postProduct, update} from "../controllers/product.controllers";
import { productIdValidator, productValidators } from "../validators/product.validators";
import { ProductRequestT } from "../types/requests.types";

const router = express.Router()

router.get(
    "/:id",
    productIdValidator,
    (req: ProductRequestT, res) => void getProduct(req, res))

router.get(
    "", 
    (req, res) => void getAllProducts(req, res))

router.post(
    "",
    ...productValidators,
    postProduct)

router.put(
    "/:id",
    ...productValidators,
    productIdValidator,
    (req: ProductRequestT, res) => void update(req, res))

export default router
