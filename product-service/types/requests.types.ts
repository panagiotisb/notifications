import {Request} from "express"
import { ProductInterface } from "./productInteface"

export interface RequestWithBodyT<T> extends Request {
    body: T
}

export type ProductRequestT = RequestWithBodyT<ProductInterface>
