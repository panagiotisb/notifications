export type UpdateParams<Type> = {
    [Key in keyof Type]: Type[Key]
}
