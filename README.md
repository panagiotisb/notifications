# Notification System (NodeJS, NextJS, TypeScript)

## Overview

This application is a notification system for users who
need to be informed for products of their interest.

The application includes two separate services. The services
read from/write to separate database instances and communicate
with each other using a broker in the Publish/Subscribe
paradigm.

### Services

The first service includes CRUD operations on ***products***
and serves as the **publisher**. When the amount of a product
changes, the service publishes to the broker to a channel
that corresponds to the product.

The second service includes a REST API for ***subscriptions***
and serves as the **subscriber**. The user uses the REST endpoint
to provide his data and subscribes to their product of interest.
When there is a product of interest published, the service
sends to all the users subscribed to this product a notification
via websockets.

## How to run

### Docker

Unless you prefer messing with the code, it is recommended to run it
using Docker (with compose) with the following command:

```bash
docker compose up 
```

This will create 5 containers: Mongo(2), Redis(1), product-service(1)
and subscription-service(1). ***Product service*** listens on port 8001
and ***subscriptions service*** listens on port 8002. Mongo instance
for products listens on 27017, for subscriptions on 27018 and Redis on
6379.

## REST Endpoints

### Production Service

| HTTP Request                      | Description             | Request Body                                                                                                              |
|:----------------------------------|-------------------------|---------------------------------------------------------------------------------------------------------------------------|
| POST /products                    | Create a new product.   | <pre lang="json">{ <br/> name: string,<br/> description: string,<br/> price: number, <br/> amount: number<br/>}</pre>     |
| GET /products/{id}                | Get a specific product. |                                                                                                                           |
| GET /products                     | Get all products.       |                                                                                                                           |
| PUT /products/{productId}         | Update a product.       | <pre lang="json">{ <br/> name: string,<br/> description: string,<br/> price: number, <br/> amount: number<br/>}</pre> |

### Subscription service

| HTTP Request           | Description                                    | Request Body                                                                                   |
|------------------------|------------------------------------------------|------------------------------------------------------------------------------------------------|
| POST /subscriptions    | User creates a new subscription to a product. | <pre lang="json">{ <br/> name: string,<br/> email: string,<br/> productId: string <br/>}</pre> |

## Notes

Connect to websocket using SocketIO and add to headers an **Authorization** field with the same value as the value in the
email you entered in the POST API of the subscription service in order to get the notification.

## Technologies

- ***Node, Express***
- ***TypeScript***
- ***Mongo***
- ***Redis***
- ***SocketIO***

## Frontend

### Frontend Overview

The frontend application consumes APIs from the services (product & subscription) and
shows notifications for the products the user subscribes (***consumer***) and the ability
to add and modify products (***producer***).

Authentication is ***mocked***, i.e. it is not used a known authentication mechanism as JWT
tokens and application server logic does not know anything about the user that makes the API
call. The request to sign as a user always suceeds, authentication state includes the user
role and the username and conditionally renders the corresponding UI.

More specifically, a producer is logged in by simply clicking the ***Sign as Producer*** from the dropdown menu on top right. Such user can add a product and change its amount. A consumer is logged
in by clicking the ***Sign as Consumer*** and then type the username in the modal that appears. This happens in order to provide the user's name as a token to the SocketIO socket's header. Such user can receive the notifications for the products that has active subscription.

### React Features

#### Context

React context is used to provide access to Authentication information, modal
utilities and socket messages to other components that need this information
for rendering.

#### Reducer

Messaging state involves complex update logic and thus is captured in **useReducer**.
This state involves a global notification for the latest message and history of messages
that can be viewed by clicking on the bell icon, which shows them as dropdown. Also,
if the user clicks on the gray background message from this bell dropdown, it marks the
message as read.

#### Hooks

Hooks used in this applcation include reusable logic for authentication information
and SocketIO messaging and take as input the corresponding state.

### Data persistence

Messages and user information are persited in the localstorage in order to preserve them
between page reloads.

### How to run frontend

You can open the application with the url <http://localhost:3000> after running
the following command:

```bash
# Tested with Node version 18.14.2
# Install using nvm:
# Optional, if it is not already installed:
nvm install 18.14.2
nvm use 18.14.2

# Run and reload frontend on changes:
npm run dev

# Or, for production build:
npm run build
npm run start
```

### Frontend Technologies

- ***NextJS, React***
- ***TailwindCSS***
