import {model, Schema} from "mongoose"
import { SubscriptionInterface, UserInterface } from "../types/subcription.types"

const userSchema = new Schema<UserInterface>({
    name: { type: String, required: true},
    email: { type: String, required: true}
})

export const User = model<UserInterface>('User', userSchema)


const subscriptionSchema = new Schema<SubscriptionInterface>({
    productId: { type: String, required: true },
    users: { type: [userSchema], required: true, ref: "User" }
})

export const Subscription = model<SubscriptionInterface>('Subscription', subscriptionSchema)
