import express, { Express } from "express"
import dotenv from 'dotenv'

import subscriptionRouter from './routes/subscription.route'
import { createMongoConnection } from "./utils/mongo"
import { socketio, sendToUsers } from "./utils/socketioUtils"
import cors from "cors"
import { connectSubscribeClient } from "./utils/redis"

const allowedOrigins = ['http://localhost:3000'];

const options: cors.CorsOptions = {
  origin: allowedOrigins
};

dotenv.config()

const app: Express = express()
app.use(cors(options))
app.use(express.json())
app.use('/subscriptions', subscriptionRouter)
const port: number = Number(process.env.PORT) ?? 8002

const httpServer = app.listen(port, () => {
  void (async () => {
    console.log(`[server] [Subscriptions] Service is running at http://localhost:${port}`)
    try {
      await createMongoConnection()
      await connectSubscribeClient()
    } catch (error) {
      console.error(error)
    }
  })()
})

const io = socketio(httpServer)
sendToUsers(io)
