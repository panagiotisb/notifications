import express from "express";
import { postSubscription } from "../controllers/subscription.controllers";
import { body } from "express-validator";
import { PostSubscriptionReqT } from "../types/requests.types";

const router = express.Router()
router.post("",
    body("name")
        .notEmpty()
        .withMessage("Name is required"),
    body("email")
        .notEmpty()
        .withMessage("Email is required"),
    body("productId")
        .notEmpty()
        .withMessage("Product ID is required."),
    (req: PostSubscriptionReqT, res) => {
        void postSubscription(req, res)
    })

export default router
