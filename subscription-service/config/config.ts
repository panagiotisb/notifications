import dotenv from 'dotenv'
import { env } from 'process'

type Optional<T> = T | undefined
const getEnvPath = (env: Optional<string>) => env
    ? env === "local" ? "" : "compose"
    : ""

const envPath = getEnvPath(process.env.NODE_ENV)

dotenv.config({
    path: `${envPath}.env`
})

export const mongoURL = () => {
    const {
        MONGO_USER,
        MONGO_PASSWORD,
        MONGO_HOST,
        MONGO_PORT
    } = env
    if (MONGO_USER === undefined || 
        MONGO_PASSWORD === undefined || 
        MONGO_HOST === undefined || 
        MONGO_PORT === undefined) {
        throw new Error("Env not set correctly.")
    }
    return `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}`
}

export const redisConfig = () => {
    const {
        REDIS_HOST,
        REDIS_PORT
    } = env
    if (REDIS_HOST === undefined || REDIS_PORT === undefined) {
        throw new Error("Redis env not set correctly")
    }
    return {
        host: REDIS_HOST,
        port: Number(REDIS_PORT)
    }
}
