import {Response} from "express"
import {createSubscription} from "../services/subscription.service";
import {subscribeToProduct} from "../services/redis.service";
import { SubscriptionInterface, UserInterface } from "../types/subcription.types";
import { validationResult } from "express-validator";
import { PostSubscriptionReqT } from "../types/requests.types";


export const postSubscription = async (req: PostSubscriptionReqT, res: Response) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(400).json(errors)
    }
    const {
        name,
        email,
        productId
    } = req.body
    const user: UserInterface = {
        name: name,
        email: email
    }
    const subscription: SubscriptionInterface = {
        productId: productId,
        users: [user]
    }

    const savedData = await createSubscription(user, subscription)
    await subscribeToProduct(savedData.productId)
    res.status(201).json(savedData)    
}
