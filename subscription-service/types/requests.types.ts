import {Request} from "express"
import { UserInterface } from "./subcription.types"

export interface RequestWithBodyT<T> extends Request {
    body: T
}

export type PostSubscriptionReqT = RequestWithBodyT<{
    productId: string
} & UserInterface>
