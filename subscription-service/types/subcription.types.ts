export interface SubscriptionInterface {
    productId: string,
    users: [UserInterface]
}

export interface UserInterface {
    name: string,
    email: string
}
