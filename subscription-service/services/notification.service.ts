import {EventEmitter} from "events";
import { SubscriptionInterface } from "../types/subcription.types";

const notificationBus = new EventEmitter()

notificationBus
    .on('product:available', (data: SubscriptionInterface) => {
        const { productId, users } = data
        users.forEach(user => {
            const {email} = user
            const payload = {productId: productId, user: user}
            notificationBus.emit(`user:send:${email}`, payload)
        })
    })
    .on('error', (error) => console.error(error))

export default notificationBus
