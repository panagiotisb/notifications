import {Subscription, User} from "../models/subscription.models";
import {HydratedDocument} from "mongoose";
import { SubscriptionInterface, UserInterface } from "../types/subcription.types";

export const createSubscription = async (user: UserInterface, subscription: SubscriptionInterface) => {
    let userOrNew = await User.findOne(user)
    if (userOrNew === null) {
        const userDocument: HydratedDocument<UserInterface> = new User(user)
        userOrNew = await userDocument.save()
    }

    return await Subscription.findOneAndUpdate({ 
        productId: subscription.productId
    }, {
        $addToSet: {
            users: userOrNew 
        }
    }, {
        new: true,
        upsert: true
    })
}
