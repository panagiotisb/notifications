import {Subscription} from "../models/subscription.models";
import notificationBus from "./notification.service";
import { redisSubscribeClient } from "../utils/redis";


export const subscribeToProduct = async (productId: string) => {
    try {
        await redisSubscribeClient.subscribe(productId, async (_message) => {
            const subscription = await Subscription.findOne({productId: productId}).orFail(new Error("Subscription not found"))
            notificationBus.emit('product:available', {
                productId: productId, users: subscription?.users
            })
        })
    } catch (error) {
        throw new Error("Subscribe error")
    }
}
