import { redisConfig } from "../config/config"
import { createClient } from "redis"

const redisSocketConfig = redisConfig()
export const redisSubscribeClient = createClient({
    socket: redisSocketConfig
})

export const connectSubscribeClient = async () => {
    return await redisSubscribeClient.connect()
}
