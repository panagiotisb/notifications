import mongoose, {connect} from "mongoose";
import { mongoURL } from "../config/config";


mongoose.set('strictQuery', false)
export const createMongoConnection = async () => {
    const mongooseConnection = await connect(mongoURL())
    mongooseConnection.connection
        .on('connected', () => {
            console.log('Database connected')
        })
        .on('error', (e) => console.error(e))
}
