import * as io from "socket.io"
import * as http from "http"
import notificationBus from "../services/notification.service";
import { Subscription, User } from "../models/subscription.models";
import { UserInterface } from "../types/subcription.types";

export const socketio = (httpServer: http.Server) => new io.Server(httpServer, {
    cors: { origin: "*" }
})

export const createMessage = (data: {productId: string, user: UserInterface}) => {
    const { productId, user } = data
    const { name, email } = user
    return `Hi ${name}, mailto: ${email}: Product ${productId} has been made available`
}

export const sendToUsers = (socketio: io.Server) => {
    socketio.on('connection', (socket) => {
        /**
         * Note: Field `token` in SocketIO header must match user's email.
         */
        const token = socket.handshake.headers.token
        if (typeof token !== "string") {
            throw Error("Please specify token as string")
        }
        notificationBus.on(`user:send:${token}`, (data: {productId: string, user: UserInterface}) => {
            const { productId, user } = data
            const message = createMessage({productId: productId, user: user})
            socket.emit('roomUser', {
                productId: productId,
                message: message
            })
            socket.once(`roomUser:${token}:${productId}`, async (answer) => {
                if (answer === "OK") {
                    const notifiedUser = await User.findOne({email: token}).exec()
                    await Subscription
                        .findOneAndUpdate({
                            productId: productId
                        }, {
                            $pull: { users: notifiedUser }
                        }, {
                            new: true
                        })
                        .exec()
                }
            })
        })
    })
}
